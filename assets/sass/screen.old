@import "normalize";
@import "mixins";
@import "fonts";
@import "curtain";

* {
    font-family: $serif;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}

html {
    font-size: 62.5%;
    /* Sets up the Base 10 stuff */
}

body {
    background: #ffffff;
    color: black;
    @include font-size(19, 29);
    position: relative;
}

/************************/
/* Global */
/************************/

header {
    color: black;
    p {
        @include font-size(21, 26);
        font-family: $sansDisplayMedium;
        text-indent: 0;
    }
    span {
        @include font-size(18, 20);
        text-transform: capitalize;
        font-family: $sansDisplayMedium;
        position: absolute;
        bottom: percentage(80 / 1376);
    }
}

h1, h2 {
    font-family: $sansDisplayMedium;
}

h1 {
    @include font-size(128, 208);
    @include reset;
    padding: 1.5rem 1rem;
    width: percentage(514 / 730);
}

h2 {
    @include font-size(36, 29);
    text-transform: capitalize;
}

p {
    text-indent: 1rem;
    margin: 0;
    font-family: $serif;
}

span {
    &.uppercase {
        font-family: $serifSmallCaps;
    }
}

ol {
    list-style-type: none;
    @include reset;
}

blockquote {
    position: relative;
    font-family: $sansDisplayBold;
    @include font-size(36, 48);
    width: percentage(550 / 800);
    margin: 0 auto;
    text-align: center;
    &.legislation {
        @extend code;
    }
}

footer {
    background-color: black;
    color: white;
    overflow: hidden;
    p {
        float: left;
    }
    #phoneHome {
        float: right;
    }
}

.wrapper {
    padding: 25px 75px;
    margin: 0 auto;
}

/************************/
/* Sections */
/************************/

section {
    position: relative;
    &.title {
        height: 1376px;
    }
}

//.sectionTop, .sectionBottom { position: absolute; }
//.sectionTop { top: 0; }
//.sectionBottom  { bottom: 0; }

// Section themes
.sky { background: url("../imgs/sky.jpg") repeat; }
.white { background: white; }


// Individual section styles
//#section2 { height: 160rem; }

#section3 .wrapper {
    padding-top: 25rem;
    blockquote {
        color: white;
    }
}

#section5 .wrapper {
    margin-left: percentage(90 / 800);
}

/************************/
/* Drones */
/************************/

#drone1, #drone2, #drone3 {
    position: absolute;
}

#drone1 {
    height: 8.5rem;
    position: absolute;
    right: percentage(30 / 800);
    top: percentage(540 / 1376);
    img {
        width: 228px;
        height: auto;
    }
}

#drone2 {
    height: 36rem;
    position: absolute;
    right: 0;
    top: -11rem;
    img {
        width: 700px;
        height: auto;
    }
}

#drone3 {
    height: 73px;
    img {
        width: 123px;
        height: auto;
        position: absolute;
        top: 150px;
        left: 50px;
    }
}

// DEV STYLES

.blockquoteSpacer {
    width: 100%;
//    height: 100%;
    position: relative;
    margin: 2rem 0;
}

.stickem-container {
  position: relative;
}

.stickit {
    margin-left: 660px;
    position: fixed;
    top: 0;
}

.stickit-end {
    bottom: 40px;
    position: absolute;
    right: 0;
}